package info.szikora.hascodeequalsbenchmarks.strategy;

import java.util.List;

import com.google.common.base.Objects;

public class GuavaSut {

    private int intField;
    private String stringField;
    private List<GuavaDto> listField;

    public GuavaSut(int intField, String stringField, List<GuavaDto> listField) {
        super();
        this.intField = intField;
        this.stringField = stringField;
        this.listField = listField;
    }

    // Equals and hashcode based upon intField and stringField and listField

    @Override
    public boolean equals(final Object other) {
        if (this == other) {
            return true;
        }
        if (other == null) {
            return false;
        }
        if (!getClass().equals(other.getClass())) {
            return false;
        }
        GuavaSut castOther = (GuavaSut) other;
        return Objects.equal(intField, castOther.intField) && Objects.equal(stringField, castOther.stringField)
                && Objects.equal(listField, castOther.listField);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(intField, stringField, listField);
    }

}
