package info.szikora.hascodeequalsbenchmarks.strategy;

import java.util.List;
import java.util.Objects;

public class Java8Sut {

    private int intField;
    private String stringField;
    private List<Java8Dto> listField;

    public Java8Sut(int intField, String stringField, List<Java8Dto> listField) {
        super();
        this.intField = intField;
        this.stringField = stringField;
        this.listField = listField;
    }

    // Equals and hashcode based upon intField and stringField and listField

    @Override
    public boolean equals(final Object other) {
        if (this == other) {
            return true;
        }
        if (other == null) {
            return false;
        }
        if (!getClass().equals(other.getClass())) {
            return false;
        }
        Java8Sut castOther = (Java8Sut) other;
        return Objects.equals(intField, castOther.intField) && Objects.equals(stringField, castOther.stringField)
                && Objects.equals(listField, castOther.listField);
    }

    @Override
    public int hashCode() {
        return Objects.hash(intField, stringField, listField);
    }

}
