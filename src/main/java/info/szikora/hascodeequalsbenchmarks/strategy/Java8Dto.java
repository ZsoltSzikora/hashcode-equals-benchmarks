package info.szikora.hascodeequalsbenchmarks.strategy;

import java.util.Objects;

public class Java8Dto {

    private boolean booleanField;
    private double doubleField;

    public Java8Dto(boolean booleanField, double doubleField) {
        super();
        this.booleanField = booleanField;
        this.doubleField = doubleField;
    }

    // Equals and hashcode based upon booleanField and doubleField

    @Override
    public boolean equals(final Object other) {
        if (this == other) {
            return true;
        }
        if (other == null) {
            return false;
        }
        if (!getClass().equals(other.getClass())) {
            return false;
        }
        Java8Dto castOther = (Java8Dto) other;
        return Objects.equals(booleanField, castOther.booleanField)
                && Objects.equals(doubleField, castOther.doubleField);
    }

    @Override
    public int hashCode() {
        return Objects.hash(booleanField, doubleField);
    }

}
