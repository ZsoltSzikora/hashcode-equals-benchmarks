package info.szikora.hascodeequalsbenchmarks.strategy;

import java.util.List;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class CommonsLang3Sut {

    private int intField;
    private String stringField;
    private List<CommonsLang3Dto> listField;

    public CommonsLang3Sut(int intField, String stringField, List<CommonsLang3Dto> listField) {
        super();
        this.intField = intField;
        this.stringField = stringField;
        this.listField = listField;
    }

    // Equals and hashcode based upon intField and stringField and listField

    @Override
    public boolean equals(final Object other) {
        if (this == other) {
            return true;
        }
        if (other == null) {
            return false;
        }
        if (!getClass().equals(other.getClass())) {
            return false;
        }
        CommonsLang3Sut castOther = (CommonsLang3Sut) other;
        return new EqualsBuilder().append(intField, castOther.intField).append(stringField, castOther.stringField)
                .append(listField, castOther.listField).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(intField).append(stringField).append(listField).toHashCode();
    }

}
