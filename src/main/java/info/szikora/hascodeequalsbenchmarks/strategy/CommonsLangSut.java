package info.szikora.hascodeequalsbenchmarks.strategy;

import java.util.List;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class CommonsLangSut {

    private int intField;
    private String stringField;
    private List<CommonsLangDto> listField;

    public CommonsLangSut(int intField, String stringField, List<CommonsLangDto> listField) {
        super();
        this.intField = intField;
        this.stringField = stringField;
        this.listField = listField;
    }

    // Equals and hashcode based upon intField and stringField and listField

    @Override
    public boolean equals(final Object other) {
        if (this == other) {
            return true;
        }
        if (other == null) {
            return false;
        }
        if (!getClass().equals(other.getClass())) {
            return false;
        }
        CommonsLangSut castOther = (CommonsLangSut) other;
        return new EqualsBuilder().append(intField, castOther.intField).append(stringField, castOther.stringField)
                .append(listField, castOther.listField).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(intField).append(stringField).append(listField).toHashCode();
    }

}
