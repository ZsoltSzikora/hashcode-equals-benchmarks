package info.szikora.hascodeequalsbenchmarks.strategy;

import java.util.List;

public class NativeEclipseSut {

    private int intField;
    private String stringField;
    private List<NativeEclipseDto> listField;

    public NativeEclipseSut(int intField, String stringField, List<NativeEclipseDto> listField) {
        super();
        this.intField = intField;
        this.stringField = stringField;
        this.listField = listField;
    }

    // Equals and hashcode based upon intField and stringField and listField

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + intField;
        result = prime * result + ((listField == null) ? 0 : listField.hashCode());
        result = prime * result + ((stringField == null) ? 0 : stringField.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        NativeEclipseSut other = (NativeEclipseSut) obj;
        if (intField != other.intField)
            return false;
        if (listField == null) {
            if (other.listField != null)
                return false;
        } else if (!listField.equals(other.listField))
            return false;
        if (stringField == null) {
            if (other.stringField != null)
                return false;
        } else if (!stringField.equals(other.stringField))
            return false;
        return true;
    }

}
