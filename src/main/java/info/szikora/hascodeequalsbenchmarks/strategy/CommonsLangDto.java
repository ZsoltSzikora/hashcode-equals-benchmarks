package info.szikora.hascodeequalsbenchmarks.strategy;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class CommonsLangDto {

    private boolean booleanField;
    private double doubleField;

    public CommonsLangDto(boolean booleanField, double doubleField) {
        super();
        this.booleanField = booleanField;
        this.doubleField = doubleField;
    }

    // Equals and hashcode based upon booleanField and doubleField

    @Override
    public boolean equals(final Object other) {
        if (this == other) {
            return true;
        }
        if (other == null) {
            return false;
        }
        if (!getClass().equals(other.getClass())) {
            return false;
        }
        CommonsLangDto castOther = (CommonsLangDto) other;
        return new EqualsBuilder().append(booleanField, castOther.booleanField)
                .append(doubleField, castOther.doubleField).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(booleanField).append(doubleField).toHashCode();
    }

}
