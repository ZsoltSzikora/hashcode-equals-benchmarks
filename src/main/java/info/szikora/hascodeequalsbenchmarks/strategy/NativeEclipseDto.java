package info.szikora.hascodeequalsbenchmarks.strategy;

public class NativeEclipseDto {

    private boolean booleanField;
    private double doubleField;

    public NativeEclipseDto(boolean booleanField, double doubleField) {
        super();
        this.booleanField = booleanField;
        this.doubleField = doubleField;
    }

    // Equals and hashcode based upon booleanField and doubleField

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (booleanField ? 1231 : 1237);
        long temp;
        temp = Double.doubleToLongBits(doubleField);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        NativeEclipseDto other = (NativeEclipseDto) obj;
        if (booleanField != other.booleanField)
            return false;
        if (Double.doubleToLongBits(doubleField) != Double.doubleToLongBits(other.doubleField))
            return false;
        return true;
    }

}
