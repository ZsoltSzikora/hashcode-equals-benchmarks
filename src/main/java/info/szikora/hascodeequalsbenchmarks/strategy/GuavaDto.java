package info.szikora.hascodeequalsbenchmarks.strategy;

import com.google.common.base.Objects;

public class GuavaDto {

    private boolean booleanField;
    private double doubleField;

    public GuavaDto(boolean booleanField, double doubleField) {
        super();
        this.booleanField = booleanField;
        this.doubleField = doubleField;
    }

    // Equals and hashcode based upon booleanField and doubleField

    @Override
    public boolean equals(final Object other) {
        if (this == other) {
            return true;
        }
        if (other == null) {
            return false;
        }
        if (!getClass().equals(other.getClass())) {
            return false;
        }
        GuavaDto castOther = (GuavaDto) other;
        return Objects.equal(booleanField, castOther.booleanField) && Objects.equal(doubleField, castOther.doubleField);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(booleanField, doubleField);
    }

}
