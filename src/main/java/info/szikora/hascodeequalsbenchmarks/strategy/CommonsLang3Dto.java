package info.szikora.hascodeequalsbenchmarks.strategy;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class CommonsLang3Dto {

    private boolean booleanField;
    private double doubleField;

    public CommonsLang3Dto(boolean booleanField, double doubleField) {
        super();
        this.booleanField = booleanField;
        this.doubleField = doubleField;
    }

    @Override
    public boolean equals(final Object other) {
        if (this == other) {
            return true;
        }
        if (other == null) {
            return false;
        }
        if (!getClass().equals(other.getClass())) {
            return false;
        }
        CommonsLang3Dto castOther = (CommonsLang3Dto) other;
        return new EqualsBuilder().append(booleanField, castOther.booleanField)
                .append(doubleField, castOther.doubleField).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(booleanField).append(doubleField).toHashCode();
    }

    // Equals and hashcode based upon booleanField and doubleField

}
