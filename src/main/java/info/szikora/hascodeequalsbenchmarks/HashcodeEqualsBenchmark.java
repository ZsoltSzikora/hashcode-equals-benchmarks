/*
 * Copyright (c) 2014, Oracle America, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 *  * Neither the name of Oracle nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package info.szikora.hascodeequalsbenchmarks;

import java.util.Arrays;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;

import info.szikora.hascodeequalsbenchmarks.strategy.CommonsLang3Dto;
import info.szikora.hascodeequalsbenchmarks.strategy.CommonsLang3Sut;
import info.szikora.hascodeequalsbenchmarks.strategy.CommonsLangDto;
import info.szikora.hascodeequalsbenchmarks.strategy.CommonsLangSut;
import info.szikora.hascodeequalsbenchmarks.strategy.GuavaDto;
import info.szikora.hascodeequalsbenchmarks.strategy.GuavaSut;
import info.szikora.hascodeequalsbenchmarks.strategy.Java8Dto;
import info.szikora.hascodeequalsbenchmarks.strategy.Java8Sut;
import info.szikora.hascodeequalsbenchmarks.strategy.NativeEclipseDto;
import info.szikora.hascodeequalsbenchmarks.strategy.NativeEclipseSut;

@State(Scope.Benchmark)
public class HashcodeEqualsBenchmark {

    NativeEclipseSut n1 = new NativeEclipseSut(42, "This is a sample string", Arrays.asList(new NativeEclipseDto[] {
            new NativeEclipseDto(true, 41d), new NativeEclipseDto(true, 42d), new NativeEclipseDto(true, 43d) }));
    NativeEclipseSut n2 = new NativeEclipseSut(42, "This is a sample string", Arrays.asList(new NativeEclipseDto[] {
            new NativeEclipseDto(true, 41d), new NativeEclipseDto(true, 42d), new NativeEclipseDto(true, 43d) }));

    CommonsLangSut c_1 = new CommonsLangSut(42, "This is a sample string", Arrays.asList(new CommonsLangDto[] {
            new CommonsLangDto(true, 41d), new CommonsLangDto(true, 42d), new CommonsLangDto(true, 43d) }));
    CommonsLangSut c_2 = new CommonsLangSut(42, "This is a sample string", Arrays.asList(new CommonsLangDto[] {
            new CommonsLangDto(true, 41d), new CommonsLangDto(true, 42d), new CommonsLangDto(true, 43d) }));

    CommonsLang3Sut c3_1 = new CommonsLang3Sut(42, "This is a sample string", Arrays.asList(new CommonsLang3Dto[] {
            new CommonsLang3Dto(true, 41d), new CommonsLang3Dto(true, 42d), new CommonsLang3Dto(true, 43d) }));
    CommonsLang3Sut c3_2 = new CommonsLang3Sut(42, "This is a sample string", Arrays.asList(new CommonsLang3Dto[] {
            new CommonsLang3Dto(true, 41d), new CommonsLang3Dto(true, 42d), new CommonsLang3Dto(true, 43d) }));

    GuavaSut g1 = new GuavaSut(42, "This is a sample string", Arrays.asList(new GuavaDto[] {
            new GuavaDto(true, 41d), new GuavaDto(true, 42d), new GuavaDto(true, 43d) }));
    GuavaSut g2 = new GuavaSut(42, "This is a sample string", Arrays.asList(new GuavaDto[] {
            new GuavaDto(true, 41d), new GuavaDto(true, 42d), new GuavaDto(true, 43d) }));

    Java8Sut j1 = new Java8Sut(42, "This is a sample string", Arrays.asList(new Java8Dto[] {
            new Java8Dto(true, 41d), new Java8Dto(true, 42d), new Java8Dto(true, 43d) }));
    Java8Sut j2 = new Java8Sut(42, "This is a sample string", Arrays.asList(new Java8Dto[] {
            new Java8Dto(true, 41d), new Java8Dto(true, 42d), new Java8Dto(true, 43d) }));

    @Benchmark
    public void testHashcodeNativeEclipseJava() {
        n1.hashCode();
    }

    @Benchmark
    public void testEqualsNativeEclipseJava() {
        n1.equals(n2);
    }

    @Benchmark
    public void testHashcodeCommonsLang() {
        c_1.hashCode();
    }

    @Benchmark
    public void testEqualsCommonsLang() {
        c_1.equals(c_2);
    }

    @Benchmark
    public void testHashcodeCommonsLang3() {
        c3_1.hashCode();
    }

    @Benchmark
    public void testEqualsCommonsLang3() {
        c3_1.equals(c3_2);
    }

    @Benchmark
    public void testHashcodeGuava() {
        g1.hashCode();
    }

    @Benchmark
    public void testEqualsGuava() {
        g1.equals(g2);
    }

    @Benchmark
    public void testHashcodeJava8() {
        j1.hashCode();
    }

    @Benchmark
    public void testEqualsJava8() {
        j1.equals(j2);
    }

}
